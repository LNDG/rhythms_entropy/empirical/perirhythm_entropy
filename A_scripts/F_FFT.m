
SetupIs = 'local';

if strcmp(SetupIs, 'tardis')
    %pn.root = '/home/mpib/LNDG/StateSwitch/WIP_eeg/S4D_MSE_CSD_v4/';
elseif strcmp(SetupIs, 'local')
    pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/F_periRhythmMSE/';
    addpath([pn.root, 'T_tools/mMSE-master'])
    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4D_MSE_CSD_v4/T_tools/fieldtrip-20170904'); ft_defaults;
end
pn.dataIn = [pn.root, 'B_data/B_periRhythmOut_mdSplit/'];
pn.dataOut  = [pn.root, 'B_data/C_FFT_Output_mdSplit/']; %mkdir(pn.dataOut)


% N = 47 YAs + 52 OAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

grandPow = [];
for indID = 1:numel(IDs)

    %% select data

    cfg_job.ID = IDs{indID};

    PeriEpisodeERP = [];

    load([pn.dataIn, sprintf('%s_PeriAlpha_Freq.mat', cfg_job.ID)])

        %% adjust input structure

        for indAmp = 1:2
        
            % implement autosandwiching
            
        % restructure: the MSE function expects {trials} (here different episodes): channel (here different metrics) + time
        temp = [];
        temp.temp1 = cat(1, PeriEpisodeERP.trial_onset{:,indAmp});
        temp.temp2 = cat(1, PeriEpisodeERP.trial_offset{:,indAmp});
        temp.temp3 = cat(1, PeriEpisodeERP.trial_onset_notch{:,indAmp});
        temp.temp4 = cat(1, PeriEpisodeERP.trial_offset_notch{:,indAmp});
        for indTrial = 1:size(temp.temp1,1)
            PeriEpisodeERP.trial{indTrial}(1,:) = [flipud(fliplr(temp.temp1(indTrial,125:250))), ...
                temp.temp1(indTrial,125:250), flipud(fliplr(temp.temp1(indTrial,125:250)))];
            PeriEpisodeERP.trial{indTrial}(2,:) = [flipud(fliplr(temp.temp2(indTrial,125:250))), ...
                temp.temp2(indTrial,125:250), flipud(fliplr(temp.temp2(indTrial,125:250)))];
            PeriEpisodeERP.trial{indTrial}(3,:) = [flipud(fliplr(temp.temp3(indTrial,125:250))), ...
                temp.temp3(indTrial,125:250), flipud(fliplr(temp.temp3(indTrial,125:250)))];
            PeriEpisodeERP.trial{indTrial}(4,:) = [flipud(fliplr(temp.temp4(indTrial,125:250))), ...
                temp.temp4(indTrial,125:250), flipud(fliplr(temp.temp4(indTrial,125:250)))];
            PeriEpisodeERP.trial{indTrial}(5,:) = [flipud(fliplr(temp.temp1(indTrial,250:250+125))), ...
                temp.temp1(indTrial,250:250+125), flipud(fliplr(temp.temp1(indTrial,250:250+125)))];
            PeriEpisodeERP.trial{indTrial}(6,:) = [flipud(fliplr(temp.temp2(indTrial,250:250+125))), ...
                temp.temp2(indTrial,250:250+125), flipud(fliplr(temp.temp2(indTrial,250:250+125)))];
            PeriEpisodeERP.trial{indTrial}(7,:) = [flipud(fliplr(temp.temp3(indTrial,250:250+125))), ...
                temp.temp3(indTrial,250:250+125), flipud(fliplr(temp.temp3(indTrial,250:250+125)))];
            PeriEpisodeERP.trial{indTrial}(8,:) = [flipud(fliplr(temp.temp4(indTrial,250:250+125))), ...
                temp.temp4(indTrial,250:250+125), flipud(fliplr(temp.temp4(indTrial,250:250+125)))];
        end                
        PeriEpisodeERP.label{1} = 'PreAlpha_pre';
        PeriEpisodeERP.label{2} = 'PostAlpha_on';
        PeriEpisodeERP.label{3} = 'PreAlpha_Notch_pre';
        PeriEpisodeERP.label{4} = 'PostAlpha_Notch_on';
        PeriEpisodeERP.label{5} = 'PreAlpha_on';
        PeriEpisodeERP.label{6} = 'PostAlpha_off';
        PeriEpisodeERP.label{7} = 'PreAlpha_Notch_on';
        PeriEpisodeERP.label{8} = 'PostAlpha_Notch_off';
        PeriEpisodeERP.label(9:end) = [];

        PeriEpisodeERP.time = [];
        for indTrial = 1:numel(PeriEpisodeERP.trial)
            PeriEpisodeERP.time{indTrial} = [-189/500:1/500:188/500];
        end

        PeriEpisodeERP.sampleinfo = [zeros(size(PeriEpisodeERP.trial,2),1), ...
            repmat(numel(PeriEpisodeERP.time{indTrial}), size(PeriEpisodeERP.trial,2),1)];%PeriEpisodeERP.sampleinfo(1:numel(PeriEpisodeERP.trial),:);

        %% setup FFT

        % The function expects fieldtrip-style input:
        % data.{trial{},elec,time{},label{},fsample,sampleInfo,trialinfo}

        % within each trial the sorting is channel*trial

        param.FFT.trial       = 'all';
        param.FFT.method      = 'mtmconvol';       % analyze entire spectrum; multitaper
        param.FFT.output      = 'pow';          % only get power values
        param.FFT.keeptrials  = 'no';           % return individual trials
        param.FFT.taper       = 'hanning';      % use hanning windows
        param.FFT.tapsmofrq   = 2;
        param.FFT.foi         = 2.^[1:.125:6.5];  % frequencies of interest
        param.FFT.t_ftimwin   = repmat(.75,1,numel(param.FFT.foi));
        param.FFT.toi         = 0;
        param.FFT.pad         = 10;

        fft = ft_freqanalysis(param.FFT, PeriEpisodeERP);
        
        grandPow(indID,indAmp,:,:,:) = fft.powspctrm;
        fft.powspctrm = grandPow;

    end
end

%% save output

save([pn.dataOut, 'FFT_allSubs.mat'], 'fft', 'param')
load([pn.dataOut, 'FFT_allSubs.mat'], 'fft', 'param')

%% linear fit (excluding 5-20 Hz range)
% shallower slope --> increasing neural irregularity

nonAlphaSub30 = find(fft.freq <5 | fft.freq >20);

linFit_2_30 = []; linFit_2_30_int = [];
for indID = 1:size(fft.powspctrm,1)
    for indAmp = 1:2
        for indCond = 1:8                
            x = log10(fft.freq(nonAlphaSub30))';
            y = log10(squeeze(fft.powspctrm(indID,indAmp,indCond,nonAlphaSub30)));
            pv=polyfit(x,y,1); % linear regression
            linFit_2_30(indID, indAmp, indCond) = pv(1);
            linFit_2_30_int(indID, indAmp, indCond) = pv(2);
        end
    end
end


%%
% 
% % on vs. pre
% figure; bar(squeeze(nanmean([linFit_2_30(:,2,1), linFit_2_30(:,2,5)],1)))
% [~, pval] = ttest(linFit_2_30(:,2,1), linFit_2_30(:,2,5))
% 
% % off vs. post
% figure; bar(squeeze(nanmean([linFit_2_30(:,2,2), linFit_2_30(:,2,6)],1)))
% [~, pval] = ttest(linFit_2_30(:,2,2), linFit_2_30(:,2,6))
% 
% % on vs. pre
% figure; bar(squeeze(nanmean([linFit_2_30(:,2,3), linFit_2_30(:,2,7)],1)))
% [~, pval] = ttest(linFit_2_30(:,2,3), linFit_2_30(:,2,7))
% 
% 
% PeriEpisodeERP.label{1} = 'PreAlpha_pre';
% PeriEpisodeERP.label{2} = 'PostAlpha_on';
% PeriEpisodeERP.label{3} = 'PreAlpha_Notch_pre';
% PeriEpisodeERP.label{4} = 'PostAlpha_Notch_on';
% PeriEpisodeERP.label{5} = 'PreAlpha_on';
% PeriEpisodeERP.label{6} = 'PostAlpha_off';
% PeriEpisodeERP.label{7} = 'PreAlpha_Notch_on';
% PeriEpisodeERP.label{8} = 'PostAlpha_Notch_off';
% 
% figure; bar(squeeze(nanmean([linFit_2_30(:,2,1), linFit_2_30(:,2,2),linFit_2_30(:,2,5), linFit_2_30(:,2,6)],1)))
% %figure; bar(squeeze(nanmean([linFit_2_30(:,2,1), linFit_2_30(:,2,5),linFit_2_30(:,2,4), linFit_2_30(:,2,8)],1)))
% [~, pval] = ttest(nanmean(linFit_2_30(:,2,[1,6]),3), nanmean(linFit_2_30(:,2,[2,5]),3))
% [~, pval] = ttest(nanmean(linFit_2_30(:,1,[1,6]),3), nanmean(linFit_2_30(:,1,[2,5]),3))
% 
% [~, pval] = ttest(nanmean(linFit_2_30(:,2,[1]),3), nanmean(linFit_2_30(:,2,[2]),3))
% [~, pval] = ttest(nanmean(linFit_2_30(:,2,[6]),3), nanmean(linFit_2_30(:,2,[5]),3))
% 
%% add within-subject standard errors & significance bars

% % new value = old value - subject average + grand average
% 
% addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/T_tools/barwitherr');
% addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/T_tools'))
% 
% h = figure('units','normalized','position',[.1 .1 .2 .25]);
% cla; hold on;
% indAmp = 2;
% freq = log10(fft.freq);
% line1 = plot(log10(fft.freq), squeeze(nanmean(nanmean(log10(fft.powspctrm(:,indAmp,[1,6],:)),3),1)), 'k--', 'LineWidth', 2)
% line2 = plot(log10(fft.freq), squeeze(nanmean(nanmean(log10(fft.powspctrm(:,indAmp,[2,5],:)),3),1)), 'k-', 'LineWidth', 2)
% xlabel('Frequency [log10 Hz]'); ylabel('log10 Power')
% xlim([freq(1), freq(end)])
% 
% % superimpose linear fits
% 
% y1 = squeeze(nanmean(linFit_2_30(:,indAmp,[1,6]),3));
% y2 = squeeze(nanmean(linFit_2_30_int(:,indAmp,[1,6]),3));
% y = [y1,y2];
% x = freq;
% y2_ls = [];
% for indID = 1:size(y,1)
%     y2_ls(indID,:) = polyval(y(indID,:),x); %y2_ls = plot(x, y2_ls, 'Color', 'k', 'LineWidth', 3);
% end
% curAverage = y2_ls(:,:);
% standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
% l1 = shadedErrorBar(x,nanmean(curAverage,1),standError, 'lineprops', {'LineStyle', '--', 'Color', 'r', 'LineWidth', 2,}, 'patchSaturation', .09);
% 
% y1 = squeeze(nanmean(linFit_2_30(:,indAmp,[2,5]),3));
% y2 = squeeze(nanmean(linFit_2_30_int(:,indAmp,[2,5]),3));
% y = [y1,y2];
% x = freq;
% y2_ls = [];
% for indID = 1:size(y,1)
%     y2_ls(indID,:) = polyval(y(indID,:),x); %y2_ls = plot(x, y2_ls, 'Color', 'k', 'LineWidth', 3);
% end
% curAverage = y2_ls(:,:);
% standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
% l2 = shadedErrorBar(x,nanmean(curAverage,1),standError, 'lineprops', {'LineStyle', '-', 'Color', 'r', 'LineWidth', 2,}, 'patchSaturation', .09);
% legend([line1, line2], {'Pre/Post'; 'Alpha on-/offset'}, 'location', 'SouthWest'); legend('boxoff')
% 
% axes('Position',[.65 .6 .2 .25])
% 
% %h = figure('units','normalized','position',[.1 .1 .2 .05]);
% %subplot(1,2,1); 
% cla; hold on;
%     subjAvg = repmat(squeeze(nanmean(linFit_2_30(1:end,2,1:8),3)),1,2,1);
%     grandAvg = repmat(squeeze(nanmean(nanmean(linFit_2_30(1:end,2,1:8),3),1)),99,2,1);
%     curData = linFit_2_30(1:end,2,1:8) - subjAvg + grandAvg;
% 
%     curData = [squeeze(curData(1:end,2,1)), squeeze(curData(1:end,2,2)),...
%         squeeze(curData(1:end,2,5)), squeeze(curData(1:end,2,6))];
% 
%     origData = [squeeze(linFit_2_30(1:end,2,1)), squeeze(linFit_2_30(1:end,2,2)),...
%         squeeze(linFit_2_30(1:end,2,5)), squeeze(linFit_2_30(1:end,2,6))];
%     
%     condPairs = [1,2; 3,4];
%     condPairsLevel = [-1.1 -1.1];
%     meanY = nanmean(curData,1);
%     errorY = nanstd(curData,[],1)/sqrt(size(curData,1));
%     %[h1, hError] = barwitherr(errorY, meanY);
%     [h1] = bar(1, meanY(1), 'FaceColor', 'k', 'EdgeColor', 'k'); [h1] = errorbar(1, meanY([1]), errorY([1]), 'Color', [0 0 0], 'LineWidth', 2);
%     [h1] = bar(2, meanY(2), 'FaceColor', 'r', 'EdgeColor', 'r'); [h1] = errorbar(2, meanY([2]), errorY([2]), 'Color', [0 0 0],'LineWidth', 2);
%     [h1] = bar(3, meanY(3), 'FaceColor', 'r', 'EdgeColor', 'r'); [h1] = errorbar(3, meanY([3]), errorY([3]), 'Color', [0 0 0],'LineWidth', 2);
%     [h1] = bar(4, meanY(4), 'FaceColor', 'k', 'EdgeColor', 'k'); [h1] = errorbar(4, meanY([4]), errorY([4]), 'Color', [0 0 0],'LineWidth', 2);
% 
%     %[h1, hError] = barwitherr(2, errorY([2]), meanY([2]));
%     for indPair = 1:size(condPairs,1)
%         % significance star for the difference
%         [~, pval] = ttest(origData(:,condPairs(indPair,1)), origData(:,condPairs(indPair,2))); % paired t-test
%         % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
%         % sigstars on top
%         if pval <.05
%             mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
%         end
%     end
%     box(gca,'off')
%     set(gca, 'XTick', []);
%     ylabel({'Slope'})
%     ylim([-1.2 -.75]); xlim([.5 4.5])
%     set(findall(gcf,'-property','FontSize'),'FontSize',18)
% 
%     pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/F_periRhythmMSE/C_figures/';
%     figureName = 'E_FFT_high';
% 
%     saveas(h, [pn.plotFolder, figureName], 'fig');
%     saveas(h, [pn.plotFolder, figureName], 'epsc');
%     saveas(h, [pn.plotFolder, figureName], 'png');
%     
% 
%     %% plot for low amplitudes
%     
% h = figure('units','normalized','position',[.1 .1 .2 .25]);
% cla; hold on;
% indAmp = 1;
% freq = log10(fft.freq);
% line1 = plot(log10(fft.freq), squeeze(nanmean(nanmean(log10(fft.powspctrm(:,indAmp,[1,6],:)),3),1)), 'k--', 'LineWidth', 2)
% line2 = plot(log10(fft.freq), squeeze(nanmean(nanmean(log10(fft.powspctrm(:,indAmp,[2,5],:)),3),1)), 'k-', 'LineWidth', 2)
% xlabel('Frequency [log10 Hz]'); ylabel('log10 Power')
% xlim([freq(1), freq(end)])
% 
% % superimpose linear fits
% 
% y1 = squeeze(nanmean(linFit_2_30(:,indAmp,[1,6]),3));
% y2 = squeeze(nanmean(linFit_2_30_int(:,indAmp,[1,6]),3));
% y = [y1,y2];
% x = freq;
% y2_ls = [];
% for indID = 1:size(y,1)
%     y2_ls(indID,:) = polyval(y(indID,:),x);
% end
% curAverage = y2_ls(:,:);
% standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
% l1 = shadedErrorBar(x,nanmean(curAverage,1),standError, 'lineprops', {'LineStyle', '--', 'Color', 'r', 'LineWidth', 2,}, 'patchSaturation', .09);
% 
% y1 = squeeze(nanmean(linFit_2_30(:,indAmp,[2,5]),3));
% y2 = squeeze(nanmean(linFit_2_30_int(:,indAmp,[2,5]),3));
% y = [y1,y2];
% x = freq;
% y2_ls = [];
% for indID = 1:size(y,1)
%     y2_ls(indID,:) = polyval(y(indID,:),x);
% end
% curAverage = y2_ls(:,:);
% standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
% l2 = shadedErrorBar(x,nanmean(curAverage,1),standError, 'lineprops', {'LineStyle', '-', 'Color', 'r', 'LineWidth', 2,}, 'patchSaturation', .09);
% legend([line1, line2], {'Pre/Post'; 'Alpha on-/offset'}, 'location', 'SouthWest'); legend('boxoff')
% 
% axes('Position',[.7 .6 .15 .25])
% 
% cla; hold on;
%     subjAvg = repmat(squeeze(nanmean(linFit_2_30(1:end,1,1:8),3)),1,2,1);
%     grandAvg = repmat(squeeze(nanmean(nanmean(linFit_2_30(1:end,1,1:8),3),1)),99,2,1);
%     curData = linFit_2_30(1:end,1,1:8) - subjAvg + grandAvg;
% 
%     curData = [squeeze(curData(1:end,1,1)), squeeze(curData(1:end,1,2)),...
%         squeeze(curData(1:end,1,5)), squeeze(curData(1:end,1,6))];
% 
%     origData = [squeeze(linFit_2_30(1:end,1,1)), squeeze(linFit_2_30(1:end,1,2)),...
%         squeeze(linFit_2_30(1:end,1,5)), squeeze(linFit_2_30(1:end,1,6))];
%     
%     condPairs = [1,2; 3,4];
%     condPairsLevel = [-.85 -.85];
%     meanY = nanmean(curData,1);
%     errorY = nanstd(curData,[],1)/sqrt(size(curData,1));
%     %[h1, hError] = barwitherr(errorY, meanY);
%     [h1] = bar(1, meanY(1), 'FaceColor', 'k', 'EdgeColor', 'k'); [h1] = errorbar(1, meanY([1]), errorY([1]), 'Color', [0 0 0], 'LineWidth', 2);
%     [h1] = bar(2, meanY(2), 'FaceColor', 'r', 'EdgeColor', 'r'); [h1] = errorbar(2, meanY([2]), errorY([2]), 'Color', [0 0 0],'LineWidth', 2);
%     [h1] = bar(3, meanY(3), 'FaceColor', 'r', 'EdgeColor', 'r'); [h1] = errorbar(3, meanY([3]), errorY([3]), 'Color', [0 0 0],'LineWidth', 2);
%     [h1] = bar(4, meanY(4), 'FaceColor', 'k', 'EdgeColor', 'k'); [h1] = errorbar(4, meanY([4]), errorY([4]), 'Color', [0 0 0],'LineWidth', 2);
% 
%     %[h1, hError] = barwitherr(2, errorY([2]), meanY([2]));
%     for indPair = 1:size(condPairs,1)
%         % significance star for the difference
%         [~, pval] = ttest(origData(:,condPairs(indPair,1)), origData(:,condPairs(indPair,2))); % paired t-test
%         % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
%         % sigstars on top
%         if pval <.05
%             mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
%         end
%     end
%     box(gca,'off')
%     set(gca, 'XTick', []);
%     ylabel({'Slope'})
%     ylim([-.87 -.77]); xlim([.5 4.5])
%     set(findall(gcf,'-property','FontSize'),'FontSize',18)
% 
%     pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/F_periRhythmMSE/C_figures/';
%     figureName = 'E_FFT_low';
% 
%     saveas(h, [pn.plotFolder, figureName], 'fig');
%     saveas(h, [pn.plotFolder, figureName], 'epsc');
%     saveas(h, [pn.plotFolder, figureName], 'png');
    
    %% add within-subject standard errors & significance bars

% new value = old value - subject average + grand average

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/T_tools/barwitherr');
addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/T_tools'))

h = figure('units','normalized','position',[.1 .1 .15 .25]);
set(h, 'renderer', 'Painters')
cla; hold on;
indAmp = 2;
freq = log10(fft.freq);
line1 = plot(log10(fft.freq), squeeze(nanmean(nanmean(log10(fft.powspctrm(:,indAmp,[1,6],:)),3),1)), 'k-', 'LineWidth', 4)
line2 = plot(log10(fft.freq), squeeze(nanmean(nanmean(log10(fft.powspctrm(:,indAmp,[2,5],:)),3),1)), 'r-', 'LineWidth', 4)
xlabel('Frequency [log10 Hz]'); ylabel('log10 Power')
xlim([freq(1), freq(end)])

% superimpose linear fits

y1 = squeeze(nanmean(linFit_2_30(:,indAmp,[1,6]),3));
y2 = squeeze(nanmean(linFit_2_30_int(:,indAmp,[1,6]),3));
y = [y1,y2];
x = freq;
y2_ls = [];
for indID = 1:size(y,1)
    y2_ls(indID,:) = polyval(y(indID,:),x); %y2_ls = plot(x, y2_ls, 'Color', 'k', 'LineWidth', 3);
end
curAverage = y2_ls(:,:);
standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
l1 = shadedErrorBar(x,nanmean(curAverage,1),standError, 'lineprops', {'LineStyle', '--', 'Color', 'k', 'LineWidth', 4}, 'patchSaturation', .09);

y1 = squeeze(nanmean(linFit_2_30(:,indAmp,[2,5]),3));
y2 = squeeze(nanmean(linFit_2_30_int(:,indAmp,[2,5]),3));
y = [y1,y2];
x = freq;
y2_ls = [];
for indID = 1:size(y,1)
    y2_ls(indID,:) = polyval(y(indID,:),x); %y2_ls = plot(x, y2_ls, 'Color', 'k', 'LineWidth', 3);
end
curAverage = y2_ls(:,:);
standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
l2 = shadedErrorBar(x,nanmean(curAverage,1),standError, 'lineprops', {'LineStyle', '--', 'Color', 'r', 'LineWidth', 4}, 'patchSaturation', .09);
legend([line1, line2], {'Pre/Post'; 'Alpha on-/offset'}, 'location', 'SouthWest'); legend('boxoff')

axes('Position',[.65 .6 .2 .25], 'YAxisLocation', 'right')

%h = figure('units','normalized','position',[.1 .1 .2 .05]);
%subplot(1,2,1); 
cla; hold on;
    subjAvg = repmat(squeeze(nanmean(linFit_2_30(1:end,2,1:8),3)),1,2,1);
    grandAvg = repmat(squeeze(nanmean(nanmean(linFit_2_30(1:end,2,1:8),3),1)),99,2,1);
    curData = linFit_2_30(1:end,2,1:8) - subjAvg + grandAvg;

    curData = [squeeze(nanmean(curData(1:end,2,[1,6]),3)), squeeze(nanmean(curData(1:end,2,[2,5]),3))];
    origData = [squeeze(nanmean(linFit_2_30(1:end,2,[1,6]),3)), squeeze(nanmean(linFit_2_30(1:end,2,[2,5]),3))];
    
    condPairs = [1,2];
    condPairsLevel = [-.975];
    meanY = nanmean(curData,1);
    errorY = nanstd(curData,[],1)/sqrt(size(curData,1));
    %[h1, hError] = barwitherr(errorY, meanY);
    [h1] = bar(1, meanY(1), 'FaceColor', 'k', 'EdgeColor', 'k'); [h1] = errorbar(1, meanY([1]), errorY([1]), 'Color', [0 0 0], 'LineWidth', 2);
    [h1] = bar(2, meanY(2), 'FaceColor', 'r', 'EdgeColor', 'r'); [h1] = errorbar(2, meanY([2]), errorY([2]), 'Color', [0 0 0],'LineWidth', 2);

    %[h1, hError] = barwitherr(2, errorY([2]), meanY([2]));
    for indPair = 1:size(condPairs,1)
        % significance star for the difference
        [~, pval] = ttest(origData(:,condPairs(indPair,1)), origData(:,condPairs(indPair,2))); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
            sigstar = mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
        end
    end
    box(gca,'off')
    set(gca, 'XTick', []);set(gca, 'YTick', [-1 -.8]);
    ylabel({''}) %ylabel({'Slope'})
    ylim([-1 -.8]); xlim([.5 2.5])
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    set(sigstar,'FontSize',15)
   
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/F_periRhythmMSE/C_figures/';
    figureName = 'E_FFT_high';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
    

%% plot for low amplitudes
    
h = figure('units','normalized','position',[.1 .1 .15 .25]);
set(h, 'renderer', 'Painters')
cla; hold on;
indAmp = 1;
freq = log10(fft.freq);
line1 = plot(log10(fft.freq), squeeze(nanmean(nanmean(log10(fft.powspctrm(:,indAmp,[1,6],:)),3),1)), 'k-', 'LineWidth', 4)
line2 = plot(log10(fft.freq), squeeze(nanmean(nanmean(log10(fft.powspctrm(:,indAmp,[2,5],:)),3),1)), 'r-', 'LineWidth', 4)
xlabel('Frequency [log10 Hz]'); ylabel('log10 Power')
xlim([freq(1), freq(end)])

% superimpose linear fits

y1 = squeeze(nanmean(linFit_2_30(:,indAmp,[1,6]),3));
y2 = squeeze(nanmean(linFit_2_30_int(:,indAmp,[1,6]),3));
y = [y1,y2];
x = freq;
y2_ls = [];
for indID = 1:size(y,1)
    y2_ls(indID,:) = polyval(y(indID,:),x);
end
curAverage = y2_ls(:,:);
standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
l1 = shadedErrorBar(x,nanmean(curAverage,1),standError, 'lineprops', {'LineStyle', '--', 'Color', 'k', 'LineWidth', 4}, 'patchSaturation', .09);

y1 = squeeze(nanmean(linFit_2_30(:,indAmp,[2,5]),3));
y2 = squeeze(nanmean(linFit_2_30_int(:,indAmp,[2,5]),3));
y = [y1,y2];
x = freq;
y2_ls = [];
for indID = 1:size(y,1)
    y2_ls(indID,:) = polyval(y(indID,:),x);
end
curAverage = y2_ls(:,:);
standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
l2 = shadedErrorBar(x,nanmean(curAverage,1),standError, 'lineprops', {'LineStyle', '--', 'Color', 'r', 'LineWidth', 4}, 'patchSaturation', .09);
legend([line1, line2], {'Pre/Post'; 'Alpha on-/offset'}, 'location', 'SouthWest'); legend('boxoff')

axes('Position',[.7 .6 .15 .25], 'YAxisLocation', 'right')

cla; hold on;
    subjAvg = repmat(squeeze(nanmean(linFit_2_30(1:end,1,1:8),3)),1,2,1);
    grandAvg = repmat(squeeze(nanmean(nanmean(linFit_2_30(1:end,1,1:8),3),1)),99,2,1);
    curData = linFit_2_30(1:end,1,1:8) - subjAvg + grandAvg;

    curData = [squeeze(nanmean(curData(1:end,1,[1,6]),3)), squeeze(nanmean(curData(1:end,1,[2,5]),3))];
    origData = [squeeze(nanmean(linFit_2_30(1:end,1,[1,6]),3)), squeeze(nanmean(linFit_2_30(1:end,1,[2,5]),3))];
    
    condPairs = [1,2];
    condPairsLevel = [-.85];
    meanY = nanmean(curData,1);
    errorY = nanstd(curData,[],1)/sqrt(size(curData,1));
    %[h1, hError] = barwitherr(errorY, meanY);
    [h1] = bar(1, meanY(1), 'FaceColor', 'k', 'EdgeColor', 'k'); [h1] = errorbar(1, meanY([1]), errorY([1]), 'Color', [0 0 0], 'LineWidth', 2);
    [h1] = bar(2, meanY(2), 'FaceColor', 'r', 'EdgeColor', 'r'); [h1] = errorbar(2, meanY([2]), errorY([2]), 'Color', [0 0 0],'LineWidth', 2);

    %[h1, hError] = barwitherr(2, errorY([2]), meanY([2]));
    for indPair = 1:size(condPairs,1)
        % significance star for the difference
        [~, pval] = ttest(origData(:,condPairs(indPair,1)), origData(:,condPairs(indPair,2))); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
            mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
        end
    end
    box(gca,'off')
    set(gca, 'XTick', []);set(gca, 'YTick', [-.83 -.8]);
    ylabel({'Slope'})
    ylim([-.83 -.8]); xlim([.5 2.5])
    set(findall(gcf,'-property','FontSize'),'FontSize',20)

    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/F_periRhythmMSE/C_figures/';
    figureName = 'E_FFT_low';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
    
%% replace bar plots with raincloudplots
    
h = figure('units','normalized','position',[.1 .1 .15 .25]);
set(h, 'renderer', 'Painters')

axes('Position',[0 .6 .25 .25], 'XAxisLocation', 'top')

cla; hold on;
    origData = [squeeze(nanmean(linFit_2_30(1:end,2,[1,6]),3)), squeeze(nanmean(linFit_2_30(1:end,2,[2,5]),3))];

 % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:2
        for j = 1:1
            data{i, j} = squeeze(origData(:,i));
            % individually demean for within-subject visualization
            data_ws{i, j} = origData(:,i)-...
                nanmean(origData(:,:),2)+...
                repmat(nanmean(nanmean(origData(:,:),2),1),size(origData(:,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = [.2 .2 .2];%cBrew(4,:);

    box off
    cla;
        h_rc = rm_raincloud(data_ws, cl,1);
        condPairs = [1,2];
        condPairsLevel = [-1.1];
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
               mysigstar_vert(gca, [condPairsLevel(indPair), condPairsLevel(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

            end
        end
        view([90 -90]);
        axis ij
    box(gca,'off')
    xlabel({'Slope'})
    xlim([-1.1 -.7]);
    curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(2)+.6*(curYTick(2)-curYTick(1))]);
    set(findall(gcf,'-property','FontSize'),'FontSize',20)

    % calculate dprime: (meanpost-meanpre)/STD(post-pre)
    dprime1 = (mean(data{2,1})-mean(data{1,1}))./std(data{2,1}-data{1,1})
    
    % get p-value
    [~, pval,ci,stats] = ttest(data{2,1}, data{1,1}); % paired t-test

axes('Position',[.5 .6 .25 .25], 'XAxisLocation', 'top')

cla; hold on;
    origData = [squeeze(nanmean(linFit_2_30(1:end,1,[1,6]),3)), squeeze(nanmean(linFit_2_30(1:end,1,[2,5]),3))];

 % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:2
        for j = 1:1
            data{i, j} = squeeze(origData(:,i));
            % individually demean for within-subject visualization
            data_ws{i, j} = origData(:,i)-...
                nanmean(origData(:,:),2)+...
                repmat(nanmean(nanmean(origData(:,:),2),1),size(origData(:,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = [.2 .2 .2];%cBrew(4,:);

    box off
    cla;
        h_rc = rm_raincloud(data_ws, cl,1);
        condPairs = [1,2];
        condPairsLevel = [-.9];
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
               mysigstar_vert(gca, [condPairsLevel(indPair), condPairsLevel(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

            end
        end
        view([90 -90]);
        axis ij
    box(gca,'off')
    xlabel({'Slope'})
    xlim([-.95 -.68]);
    curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(2)+.6*(curYTick(2)-curYTick(1))]);
    set(findall(gcf,'-property','FontSize'),'FontSize',20)

    % calculate dprime: (meanpost-meanpre)/STD(post-pre)
    dprime1 = (mean(data{2,1})-mean(data{1,1}))./std(data{2,1}-data{1,1})
    
    % get p-value
    [~, pval,ci,stats] = ttest(data{2,1}, data{1,1}); % paired t-test
    
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/F_periRhythmMSE/C_figures/';
    figureName = 'E_FFT_RCP';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
% Merge episode indices across episodes

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/';
pn.preprocData  = [pn.root, 'D_eBOSC_noDur/B_data/A_dataPreBOSC/'];
pn.dataBOSC     = [pn.root, 'D_eBOSC_noDur/B_data/B_eBOSCout/'];
pn.dataOut      = [pn.root, 'F_periRhythmMSE/B_data/A_eventTiming_mdSplit/'];

% N = 47 YAs + 52 OAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

for indID = 1:numel(IDs)
    load([pn.dataOut, IDs{indID}, '_EpisodeTiming_mdSplit.mat'], 'Event*')
    for indAmp = 1:2
        for indChan = 1:60
                MergedOnsets{indID, indAmp,indChan} = cat(2, EventOnset_s{indAmp,indChan});
                MergedOffsets{indID, indAmp,indChan} = cat(2, EventOffset_s{indAmp,indChan});
                MergedDurations{indID, indAmp,indChan} = cat(2, EventDuration_s{indAmp,indChan});
                MergedAmps{indID, indAmp,indChan} = cat(2, EventAmp{indAmp,indChan});
        end
    end
end % subject

chans = [44:50, 52:56, 58:60];

figure;
subplot(2,2,1); histogram(cat(2, MergedOnsets{:,1,chans,:}),100); title('Onset time histogram');
hold on; histogram(cat(2, MergedOnsets{:,2,chans,:}),100);
subplot(2,2,2); histogram(cat(2, MergedOffsets{:,1,chans,:}),100); title('Offset time histogram');
hold on; histogram(cat(2, MergedOffsets{:,2,chans,:}),100);
subplot(2,2,3); histogram(cat(2, MergedDurations{:,1,chans,:}),1000); xlim([0 2]); title('Duration histogram');
hold on; histogram(cat(2, MergedDurations{:,2,chans,:}),1000);
subplot(2,2,4); histogram(log10(cat(2, MergedAmps{:,1,chans,:})),100); title('Amplitude histogram (log10)');
hold on; histogram(cat(2, MergedAmps{:,2,chans,:}),100);
